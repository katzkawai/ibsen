# INSTALLATION PYTHON3.5 UBUNTU
if [ $(dpkg-query -W -f='${Status}' python3.5 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
	echo "not installed"
	sudo add-apt-repository ppa:fkrull/deadsnakes
	sudo apt-get update
	sudo apt-get install python3.5 
	sudo apt-get install python3.5-dev  
else
	echo "python3.5 already installed"
fi

cd ..
mkdir venv
virtualenv -p python3.5 venv
source venv/bin/activate
cd oTree
pip3.5 install -r requirements.txt
pip3.5 install -U ../otree-core
otree resetdb






