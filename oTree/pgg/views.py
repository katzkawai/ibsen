# -*- coding: utf-8 -*-
from __future__ import division

from otree.common import Currency as c, currency_range, safe_json

from . import models
from ._builtin import Page, WaitPage
from .models import Constants

import random

def vars_for_all_templates(self):
    return {
            'Instructions': 'PGG/Instructions.html',
            'min_rounds': Constants.num_rounds - 5,
            'max_rounds': Constants.num_rounds + 5,
            'num_groups': len(self.subsession.get_groups()),
    }

class Contribute(Page):

    timeout_seconds = 8

    form_model = models.Player
    form_fields = ['contribution']

    # If participipant is advanced by the experimenter
    def before_next_page(self):
        if self.timeout_happened:
            self.player.automatic_decision = True
            self.player.contribution = random.choice(range(0,Constants.endowment + 1,2))

        
         
class ResultsWaitPage(WaitPage):
    wait_for_all_groups = True

    #Los players avanzados automaticamente no pasan por aqui?

    def after_all_players_arrive(self):
        for g in self.subsession.get_groups():

            #Count the number of timeouts within each group
            num_timeouts = 0
            for p in g.get_players():
                if p.automatic_decision == True:
                    num_timeouts = num_timeouts + 1
            
            g.num_timeouts = int(num_timeouts)


            #Now, I can set the payoffs
            g.set_payoffs()

            for q in g.get_players():
                q.participant.vars['cumulative_payoff']=sum([p.payoff for p in q.in_all_rounds()])   
                #Update player variable to be exported
                p.cumulative_payoff = p.participant.vars['cumulative_payoff'] 
                p.participant.vars['cumulative_payoff_PGG'] = p.participant.vars['cumulative_payoff'] 
        
        if (self.group.treatment == 'group'):

            list_groups = [item for item in self.subsession.get_groups() if item.treatment=='group']
            
            # For each group we compute the groupal payoff
            for g in list_groups:
                groupal_payoff = sum([p.participant.vars['cumulative_payoff'] for p in g.get_players()])
                g.groupal_payoff = groupal_payoff

            sorted_groups = sorted(list_groups,key=lambda group: group.groupal_payoff, reverse = True)

            initial = 6000
            for g in sorted_groups:
                g.artificial_payoff = c(initial)
                initial = c(initial - 1000)

            for g in list_groups:
                for q in g.get_players():
                    q.participant.vars['groupal_payoff'] = groupal_payoff

        #             # Automated players get zero as groupal payoff for this round
        #             if q.automatic_decision == True:
        #                 q.payoff = 0
        #                 q.participant.vars['cumulative_payoff']=sum([p.payoff for p in q.in_all_rounds()])
        #                 groupal_payoff = sum([p.participant.vars['cumulative_payoff'] for p in g.get_players()])
        #                 q.participant.vars['groupal_payoff']= groupal_payoff

        # # Automated players get nothing in the automated round
        # for q in self.subsession.get_players():
        #     if q.automatic_decision == True:
        #         q.payoff = 0
        #         q.participant.vars['prev_payoff']=q.payoff
        #         q.participant.vars['cumulative_payoff']=sum([p.payoff for p in q.in_all_rounds()])   
        #         #Update player variable to be exported
        #         q.cumulative_payoff = q.participant.vars['cumulative_payoff'] 



        if self.subsession.round_number == Constants.num_rounds:
            self.subsession.set_hierarchy()

class FirstWait(WaitPage):
    wait_for_all_groups = True

    def is_displayed(self):
        return self.subsession.round_number == 1 

class FaseI(Page):

    def is_displayed(self):
        return self.subsession.round_number == 1 
        
               
class FinalResult(Page):
    def is_displayed(self):
        return self.subsession.round_number == Constants.num_rounds 

class GeneralInstructions(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 

class QuestionsI1(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 

    form_model = models.Player
    form_fields = ['questionsI1']

class AnswerQuestionsI1(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 


class QuestionsI2(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 

    form_model = models.Player
    form_fields = ['questionsI2']

class AnswerQuestionsI2(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 


class QuestionsI3(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 

    form_model = models.Player
    form_fields = ['questionsI3']

class AnswerQuestionsI3(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 


class QuestionsI4(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 

    form_model = models.Player
    form_fields = ['questionsI4']

class AnswerQuestionsI4(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 

class QuestionsI5(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 

    form_model = models.Player
    form_fields = ['questionsI5']

class AnswerQuestionsI5(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 


class QuestionsI6(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 

    form_model = models.Player
    form_fields = ['questionsI6']

class AnswerQuestionsI6(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 




class QuestionsII7(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 
        
    form_model = models.Player
    form_fields = ['questionsII7']

class AnswerQuestionsII7(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 


class QuestionsII8(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 
        
    form_model = models.Player
    form_fields = ['questionsII8']

class AnswerQuestionsII8(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 


class QuestionsII9(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 
        
    form_model = models.Player
    form_fields = ['questionsII9']

class AnswerQuestionsII9(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 

class QuestionsII10(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 
        
    form_model = models.Player
    form_fields = ['questionsII10']

class AnswerQuestionsII10(Page):
    timeout_seconds = 12
    def is_displayed(self):
        return self.subsession.round_number == 1 

page_sequence = [
    GeneralInstructions,
    QuestionsI1,
    AnswerQuestionsI1,
    QuestionsI2,
    AnswerQuestionsI2,
    QuestionsI3,
    AnswerQuestionsI3,
    QuestionsI4,
    AnswerQuestionsI4,
    QuestionsI5,
    AnswerQuestionsI5,
    QuestionsI6,
    AnswerQuestionsI6,
    QuestionsII7,
    AnswerQuestionsII7,
    QuestionsII8,
    AnswerQuestionsII8,
    QuestionsII9,
    AnswerQuestionsII9,
    QuestionsII10,
    AnswerQuestionsII10,
    FaseI,
    FirstWait,
    Contribute,
    ResultsWaitPage,
    FinalResult
]
