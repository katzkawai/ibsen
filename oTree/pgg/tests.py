# -*- coding: utf-8 -*-
from __future__ import division

import random

from otree.common import Currency as c, currency_range

from . import views
from ._builtin import Bot
from .models import Constants


class PlayerBot(Bot):
    #"" two rounds ""


    def play_round(self):
    	# round 1 

    	if self.subsession.round_number == 1:

            self.submit(views.GeneralInstructions)
            self.submit(views.QuestionsI1, {'questionsI1': random.choice([0,5,10,15])})
            self.submit(views.AnswerQuestionsI1)
            self.submit(views.QuestionsI2, {'questionsI2': random.choice(range(0, 30,1))})
            self.submit(views.AnswerQuestionsI2)
            self.submit(views.QuestionsI3, {'questionsI3': random.choice(range(0, 30,1))})
            self.submit(views.AnswerQuestionsI3)
            self.submit(views.QuestionsI4, {'questionsI4': random.choice(range(0, 30,1))})
            self.submit(views.AnswerQuestionsI4)
            self.submit(views.QuestionsI5, {'questionsI5': random.choice(['A','B','C','D'])})
            self.submit(views.AnswerQuestionsI5)
            self.submit(views.QuestionsI6, {'questionsI6': random.choice(['A','B','C','D'])})
            self.submit(views.AnswerQuestionsI6)
            self.submit(views.QuestionsII7, {'questionsII7': random.choice(range(0, 20,1))})
            self.submit(views.AnswerQuestionsII7)
            self.submit(views.QuestionsII8, {'questionsII8': random.choice(range(0, 20,1))})
            self.submit(views.AnswerQuestionsII8)
            self.submit(views.QuestionsII9, {'questionsII9': random.choice(range(0, 20,1))})
            self.submit(views.AnswerQuestionsII9)
            self.submit(views.QuestionsII10, {'questionsII10': random.choice(range(0, 20,1))})
            self.submit(views.AnswerQuestionsII10)
            self.submit(views.FaseI)



        #All rounds
        self.submit(views.Contribute, {'contribution': random.choice(range(0,Constants.endowment + 1,2))})

  
        #Final round
        if self.subsession.round_number == Constants.num_rounds:
        	self.submit(views.FinalResult)
        

    def validate_play(self):
        pass
