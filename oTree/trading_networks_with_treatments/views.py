from otree.api import Currency as c, currency_range
from . import models
from ._builtin import Page, WaitPage
from .models import Constants
from django.utils import timezone
from django.core.cache import cache

import datetime
import math
import random
import logging
from django.utils.translation import ugettext_lazy as _
logger = logging.getLogger('django')


class WaitStartPage(WaitPage):
    def is_displayed(self):
        return self.subsession.round_number == 1 

    
class Instructions(Page):
    
    #def set_extra_attributes(self):
    #    self.timeout_seconds = (self.subsession.created_at + datetime.timedelta(seconds=20 * self.subsession.round_number) - timezone.now()).total_seconds()

    def vars_for_template(self):
        return {
            'treatment': self.session.config['treatment'],
            'role': self.player.role(),
        }

    def is_displayed(self):
        return self.subsession.round_number == 1 


class InstructionsWaitPage(WaitPage):
    #body_main
    def is_displayed(self):
        return self.subsession.round_number == 1 
    def after_all_players_arrive(self):
        pass


class ResultsWaitPage(WaitPage):
    
    def after_all_players_arrive(self):
        t1 = datetime.datetime.now()
        self.group.set_payoffs()
        logger.info("payoff {0}".format(datetime.datetime.now() - t1))
        


class Results(Page):
    
    form_model = models.Player
    form_fields = ['overrun']
    
    def set_extra_attributes(self):
        if self.player.role() != "intermediary":
            self.timeout_seconds = 1
        elif self.subsession.round_number != 1:
            self.timeout_seconds = 60 - (timezone.now() - self.group.in_round(self.subsession.round_number-1).updated_at).total_seconds()       
        else:
            self.timeout_seconds = 60

    
    def vars_for_template(self):
        t1 = datetime.datetime.now()
        selected_path = ""
        round_number = self.round_number % 15 if self.round_number % 15 != 0 else 15
        experiment_number = int(self.round_number / 15.01) + 1
        if self.subsession.round_number % 15 == 1:
            nodes = cache.get(str(self.session.id) + "r" + str(self.round_number) + "g" +  str(self.group.id_in_subsession) + "nodes")    
            edges = cache.get(str(self.session.id) + "r" + str(self.round_number)  + "g" +  str(self.group.id_in_subsession) + "edges")    
            data = {"nodes": nodes, "edges": edges}
            logger.info("RESULTS {0} {1}".format(self.participant._id_in_session(), datetime.datetime.now() - t1))
            return {
                'result': False,
                'round_number': round_number,
                'experiment_number': experiment_number,
                'data': data,    
                'player_path': [],
                'path': [],
                'id_in_group': str(self.player.id_in_group),
                'player_color': Constants.colors['player'],
                'you_label': _("you")
            }

        else:
            nodes = cache.get(str(self.session.id) + "r" + str(self.round_number -1) + "g" +  str(self.group.id_in_subsession) + "nodes")    
            edges = cache.get(str(self.session.id) + "r" + str(self.round_number -1) + "g" +  str(self.group.id_in_subsession) + "edges")    
            data = {"nodes": nodes, "edges": edges}
            if self.group.in_round(self.subsession.round_number -1).path:
                path = list(self.group.in_round(self.subsession.round_number -1).path[1:-1].replace(' ', '').split(','))
                for node in path:
                    selected_path += ", Player " + str(int(node))
                selected_path = selected_path[1:]
            logger.info("RESULTS {0} {1}".format(self.participant._id_in_session(), datetime.datetime.now() - t1))
            previous_round_group = self.group.in_round(self.subsession.round_number -1)
            previous_round_player = self.player.in_round(self.subsession.round_number -1)
            return {
                'result': True,
                'round_number': round_number,
                'experiment_number': experiment_number,
                'data': data,    
                'deal': previous_round_group.deal,
                'payoff': '%.2f EUR' % previous_round_player.payoff,
                'selected_path': selected_path,
                'path': previous_round_group.path,
                'player_path': previous_round_player.path,
                'total_cost': '%.2f EUR' % previous_round_group.total_cost,
                'id_in_group': str(self.player.id_in_group),
                'p_total_cost': '%.2f EUR' % previous_round_player.total_cost,
                'player_color': Constants.colors['player'],
                'you_label': _("you")
            }
            
    def before_next_page(self):
        if self.timeout_happened:
            if self.player.role() == "intermediary":
                if self.round_number % 15 == 1:
                    self.player.overrun = random.randint(0,100)
                else:
                    self.player.overrun = self.player.in_round(self.round_number-1).overrun 
            else:
                self.player.overrun = 0


class ShowMoneyWaitPage(WaitPage):
    wait_for_all_groups = True

    def is_displayed(self):
        return self.subsession.round_number == Constants.num_rounds
    def after_all_players_arrive(self):
        if self.subsession.round_number == Constants.num_rounds:
            total_payoff = 0
            for p in self.session.get_participants():
                total_payoff += p.payoff
            if total_payoff == 0:
                self.session.config['real_world_currency_per_point'] = 0
            else:
                self.session.config['real_world_currency_per_point'] = self.session.config['budget'] / total_payoff
            
class ShowMoney(Page):
    def is_displayed(self):
        return self.subsession.round_number == Constants.num_rounds



page_sequence = [
    WaitStartPage,
    Instructions,
    InstructionsWaitPage,
    Results,
    ResultsWaitPage,
    ShowMoneyWaitPage,
    ShowMoney,
]
