# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import otree.db.models
import otree_save_the_change.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('otree', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_is_missing_players', otree.db.models.BooleanField(default=False, db_index=True, choices=[(True, 'Yes'), (False, 'No')])),
                ('id_in_subsession', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('real_price', otree.db.models.FloatField(null=True)),
                ('session', otree.db.models.ForeignKey(related_name='forecast_group', to='otree.Session')),
            ],
            options={
                'db_table': 'forecast_group',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_index_in_game_pages', otree.db.models.PositiveIntegerField(default=0, null=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('id_in_group', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('training_question_1', otree.db.models.CharField(max_length=500, null=True, choices=[(b'0', b'0'), (b'1200', b'1200'), (b'1299', b'1299'), (b'1350', b'1350')])),
                ('training_question_2', otree.db.models.CharField(max_length=500, null=True, choices=[(b'Period 10', b'Period 10'), (b'Period 20', b'Period 20')])),
                ('pe', otree.db.models.PositiveIntegerField(null=True)),
                ('cash', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('period_points', otree.db.models.FloatField(null=True)),
                ('total_points', otree.db.models.FloatField(default=0.0, null=True)),
                ('group', otree.db.models.ForeignKey(to='forecast.Group', null=True)),
                ('participant', otree.db.models.ForeignKey(related_name='forecast_player', to='otree.Participant')),
                ('session', otree.db.models.ForeignKey(related_name='forecast_player', to='otree.Session')),
            ],
            options={
                'db_table': 'forecast_player',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Subsession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('session', otree.db.models.ForeignKey(related_name='forecast_subsession', to='otree.Session', null=True)),
            ],
            options={
                'db_table': 'forecast_subsession',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.AddField(
            model_name='player',
            name='subsession',
            field=otree.db.models.ForeignKey(to='forecast.Subsession'),
        ),
        migrations.AddField(
            model_name='group',
            name='subsession',
            field=otree.db.models.ForeignKey(to='forecast.Subsession'),
        ),
    ]
