# -*- coding: utf-8 -*-
from __future__ import division

from otree.common import Currency as c, currency_range, safe_json

from . import models
from ._builtin import Page, WaitPage
from .models import Constants
from otree.common import safe_json
from django.utils import timezone
import random
import datetime

def vars_for_all_templates(self):
	return {
		'total_q': 2,
		'round_num': self.subsession.round_number,
		'num_of_rounds': Constants.num_rounds,  # no of periods
		'num_participants': Constants.players_per_group,
	}


class Instruction(Page):

	timeout_seconds = 30
	template_name = 'forecast/Instructions.html'
	def is_displayed(self):
		return self.subsession.round_number == 1

class ExperimentWaitPage(Page):

	def is_displayed(self):
		return self.subsession.round_number == 1

	def body_text(self):
		return 'Waiting for the other participants to join the experiment.'


class Question1(Page):

	def is_displayed(self):
		return self.subsession.round_number == 1

	template_name = 'forecast/Question.html'

	form_model = models.Player
	form_fields = ['training_question_1']

	def vars_for_template(self):
		return {'num_q': 1}

	timeout_seconds = 20

class Question2(Page):

	timeout_seconds = 20

	def is_displayed(self):
		return self.subsession.round_number == 1

	template_name = 'forecast/Question.html'

	form_model = models.Player
	form_fields = ['training_question_2']

	def vars_for_template(self):
		return {'num_q': 2}

	timeout_seconds = 20

class Feedback1(Page):

	timeout_seconds = 20

	def is_displayed(self):
		return self.subsession.round_number == 1

	template_name = 'forecast/Feedback.html'

	def vars_for_template(self):
		return {'num_q': 1,
				'question': 'Suppose in one period, your prediction for the market price is 45.5, and the market price turns out to be 45.7, how many points do you earn in this period round (round it for the nearest integer)?',
				}

class Feedback2(Page):

	timeout_seconds = 20

	def is_displayed(self):
		return self.subsession.round_number == 1

	template_name = 'forecast/Feedback.html'

	def vars_for_template(self):
		return {'num_q': 2,
				'question': 'Suppose a financial advisor predicts that the asset price goes up in period 10, and goes down in period 20, and his pension fund acts according to his prediction. In which period does the fund buy more units of asset?',
				}

class FeedbackWaitPage(WaitPage):
	def body_text(self):
		return 'Waiting for the other participants to complete the tutorial'

	def is_displayed(self):
		return self.subsession.round_number == 1

	def after_all_players_arrive(self):
		self.group.updated_at = timezone.now()


class ResultsWaitPage(WaitPage):

	def body_text(self):
		return 'Waiting for the other participant to choose.'

	def after_all_players_arrive(self):
		self.group.set_real_price()

		for p in self.group.get_players():
			p.set_payoff()
		if self.subsession.round_number < Constants.num_rounds:	
			next_group = self.group.in_round(self.subsession.round_number + 1)
			next_group.updated_at = timezone.now()


class Results(Page):

	form_model = models.Player
	form_fields = ['pe']

	def set_extra_attributes(self):
		self.timeout_seconds = (self.group.updated_at + datetime.timedelta(seconds=30) - timezone.now()).total_seconds()

	def set_mail(self):
		self.timeout_mailing_seconds = 3
		self.mail_body_text = "A new round has started. Please take a moment to play " + self.request.build_absolute_uri(self.participant._current_form_page_url)
	
	def vars_for_template(self):
		if self.subsession.round_number == 1:
			return {
				'real_price': safe_json([0]),
				'my_estimation': safe_json([0]),
				'round_list': safe_json([0]),
				'rounds': self.player.in_all_rounds(),
				'current_round': self.subsession.round_number
			}
		else:

			self.player.set_payoff()

			real_price_list = []
			my_estimation_list = []
			round_list = []

			for p in self.player.in_previous_rounds():
				real_price_list.append(p.group.real_price or 0)
				my_estimation_list.append(p.pe)
				round_list.append(p.round_number)
			
			return {
				'real_price': safe_json(real_price_list),
				'my_estimation': safe_json(my_estimation_list),
				'round_list': safe_json(round_list),
				'rounds': self.player.in_previous_rounds(),
				'current_round': self.subsession.round_number
			}

	def before_next_page(self):
		if self.timeout_happened:
			self.player.pe = random.randint(30,50)
			
class Payment(Page):

	def is_displayed(self):
		return self.subsession.round_number == Constants.num_rounds

	def total_payoff(self):
		return self.player.participant.total_pay()


	def vars_for_template(self):

		return {
			'payoff_in_session': self.player.participant.payoff.to_real_world_currency(self.session),
			'total_payoff': self.player.participant.total_pay(),
			
			
		}
	

		

		
		



page_sequence = [
	#ExperimentWaitPage,
	Instruction,
	Question1,
	Feedback1,
	Question2,
	Feedback2,
	FeedbackWaitPage,
	Results,
	ResultsWaitPage,
	Payment
]
