# urls.py
from django.conf.urls import url
from otree.default_urls import urlpatterns

urlpatterns.append(url(r'^send_message$', 'chat_channels.custom_views.send_message'))
urlpatterns.append(url(r'^get_messages$', 'chat_channels.custom_views.get_messages'))