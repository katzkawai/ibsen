from django.http import HttpResponse
from . import models
from django.core import serializers
import json
import datetime
def send_message(request):
	message = request.GET['message']
	player_id = request.GET['player_id']
	group_id = request.GET['group_id']
	round_number = request.GET['round_number']

	db_message = models.Message(player_id=player_id, group_id=group_id, data=message, round_number=round_number)
	db_message.save()

	result = {'status': 'OK'}

	return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")

def get_messages(request):
	t1 = datetime.datetime.now()
	group_id = request.GET['group_id']
	round_number = request.GET['round_number']

	messages = models.Message.objects.filter(round_number=round_number, group_id= group_id).order_by('id').all()
	
	messages = serializers.serialize("json",messages,fields=('id','player', 'data', 'created_at'), use_natural_foreign_keys=True)

	print ("TIEMPO DE CONSULTA", datetime.datetime.now() - t1)
	return HttpResponse(messages, content_type="application/json")