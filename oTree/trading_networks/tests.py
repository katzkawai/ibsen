from otree.api import Currency as c, currency_range, Submission
from . import views
from ._builtin import Bot
from .models import Constants

import random


class PlayerBot(Bot):

    def play_round(self):
        if self.subsession.round_number == 1:
            #yield Submission(views.WaitStartPage, check_html=False)
            yield (views.Instructions)
        if self.subsession.round_number == 5:   
            yield (views.InstructionsSecondPhase)
        
        if self.player.role() not in ["source","destination"]:
            yield(views.Decision,
                 {
                    'overrun': random.randint(1,100)
                })
        else:
           yield Submission(views.Decision, {'overrun': 0}, check_html=False)

        yield(views.Results)
