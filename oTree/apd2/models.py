# -*- coding: utf-8 -*-
# <standard imports>
from __future__ import division
from otree.db import models
from otree.constants import BaseConstants
from otree.models import BaseSubsession, BaseGroup, BasePlayer, BaseLink

from otree import widgets
from otree.common import Currency as c, currency_range
import random
# </standard imports>

author = 'M. Pereda'

doc = """
Hierachy Experiment II - Prisoner's Dilemma
"""


keywords = ("Prisoner's Dilemma",)

class Subsession(BaseSubsession):

    pairings = [[0,1,6,7,12,13,18,19,2,8,3,14,4,20,10,15,9,21,16,22,5,17,11,23], [2,3,8,9,14,15,20,21,0,6,1,12,5,18,7,16,10,22,13,23,4,11,17,19], [4,5,10,11,16,17,22,23,3,9,2,13,1,19,6,12,7,18,15,20,8,14,0,21], [0,15,1,7,2,22,3,23,4,16,5,10,6,19,8,20,9,13,11,17,12,18,14,21], [0,3,1,4,2,5,6,9,7,10,8,11,12,13,14,15,16,17,18,19,20,21,22,23]]
    #Uncoment to test with 6 players
    #pairings = [[0,2,1,3,4,5], [0,5,1,4,2,3]]
    
    def before_subsession_starts(self):
        if self.round_number == 1:
            for p in self.get_players():
                p.participant.vars['cumulative_payoff2'] = 0
                p.participant.vars['stage_payoff']=0 
                p.participant.vars['prev_payoff']=0 

                #This just to start an isolated APD with no preceding PGG
                if not 'cumulative_payoff' in p.participant.vars:
                    p.participant.vars['cumulative_payoff_PGG'] = 0
                if not 'groupal_payoff' in p.participant.vars:
                    p.participant.vars['groupal_payoff'] = 0
                    

class Constants(BaseConstants):
    name_in_url = 'APD2'
    players_per_group = 2
    num_phases = 5
    num_rounds_per_phase = 10
    num_rounds = num_phases * num_rounds_per_phase
    Show_up_fee = 5


    # Payoffs
    Y_X_amount = 4 #Not in points if i don't use c()
    X_amount = 3
    X_Y_amount = 1
    Y_amount = 2

    #From game template
    base_points = c(0) # Zero because we are not using it

    #Multipliers per hierarchy level
    A_multiplier = 5
    B_multiplier = 4
    C_multiplier = 3
    D_multiplier = 2




class Group(BaseGroup):
    num_timeouts = models.IntegerField()

class Link(BaseLink):
    pass

class Player(BasePlayer):

    decision = models.CharField(
        # choices is specify in the Decision Page in views.py
        #choices=['X', 'Y'],
        doc="""This player's decision""",
        widget=widgets.RadioSelectHorizontal(),
        verbose_name='¿Cuál es tu decisión?',
    )

    def other_player(self):
        return self.get_others_in_group()[0]

    def set_payoff(self):
        
        points_matrix = {'X': {'X': c(Constants.X_amount),
                                       'Y': c(Constants.X_Y_amount)},
                         'Y':   {'X': c(Constants.Y_X_amount),
                                      'Y': c(Constants.Y_amount)}}

        self.payoff = (points_matrix[self.decision]
                                           [self.other_player().decision])

    # I define player variables to store the values to be exported since participant variables are not yet exported in this otree version
    cumulative_payoff = models.CurrencyField()   
    profile = models.CharField()
    partner_profile = models.CharField()
    partner_ID = models.PositiveIntegerField()
    multiplier = models.IntegerField()
    stage_payoff = models.CurrencyField() 
    automatic_decision = models.BooleanField()
    pairings = models.CharField()

    index = models.IntegerField()
    partner_index = models.IntegerField()

    myID = models.PositiveIntegerField()


