��          T      �       �      �      �      �      �      �      	  k    �   �  �     P  �       
     *                                           instructions_p1 instructions_p2 instructions_p3 instructions_title welcome_text welcome_title Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-03-31 14:27+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Crea tu aplicación siguiendo el método usual 'otree startapp app_name' . Crea un directorio llamado 'locale' dentro del directorio app_name Observa en el código fuente de esta página como se definen las cadenas a traducir y como se detecta el lenguaje del navegador de un cliente y escribe tu app. Lanza el comando 'django-admin makemessages --locale=en' dentro de la carpeta tu app si quieres que el inglés esté soportado. Este comando creará una carpeta 'en' dentro del directorio 'locale'. Para cada msgid, rellena el campo msgstr con la correspondiente traducción. Por último, lanza el comando 'django-admin compilemessages'. Instrucciones Bienvenido Tu primera aplicación IBSEN multilenguage 