from otree.api import Currency as c, currency_range
from . import models
from ._builtin import Page, WaitPage
from .models import Constants
from django.utils import translation


class WelcomePage(Page):
	#def is_displayed(self):
	#	if 'HTTP_ACCEPT_LANGUAGE' in self.request.META:
	#		language_code = self.request.META['HTTP_ACCEPT_LANGUAGE'].split(',')[0]
	#		translation.activate(language_code)
	#	return True
	pass
	
class InstructionsPage(Page):
	#def is_displayed(self):
	#	if 'HTTP_ACCEPT_LANGUAGE' in self.request.META:
	#		language_code = self.request.META['HTTP_ACCEPT_LANGUAGE'].split(',')[0]
	#		translation.activate(language_code)
	#	return True
	pass
page_sequence = [
	WelcomePage,
	InstructionsPage,
]
