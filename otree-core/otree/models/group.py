#!/usr/bin/env python
# -*- coding: utf-8 -*-

from otree_save_the_change.mixins import SaveTheChange

from otree.db import models
from otree.common_internal import get_models_module
from otree.models.fieldchecks import ensure_field
import django.core.exceptions
import copy

ATTRIBUTE_ERROR_MESSAGE = '''
Group object has no attribute '{}'. If it is a model field or method,
it must be declared on the Group class in models.py.
'''.replace('\n', '')


class BaseGroup(SaveTheChange, models.Model):
    """Base class for all Groups.
    """

    class Meta:
        abstract = True
        index_together = ['session', 'id_in_subsession']
        ordering = ['pk']

    def __getattribute__(self, name):
        try:
            return super(BaseGroup, self).__getattribute__(name)
        except AttributeError:
            raise AttributeError(ATTRIBUTE_ERROR_MESSAGE.format(name)) from None

    id_in_subsession = models.PositiveIntegerField(db_index=True)

    session = models.ForeignKey(
        'otree.Session', related_name='%(app_label)s_%(class)s'
    )

    round_number = models.PositiveIntegerField(db_index=True)

    def __unicode__(self):
        return str(self.pk)

    def get_players(self):
        return list(self.player_set.order_by('id_in_group'))

    def get_player_by_id(self, id_in_group):
        try:
            return self.player_set.get(id_in_group=id_in_group)
        except django.core.exceptions.ObjectDoesNotExist:
            raise ValueError(
                'No player with id_in_group {}'.format(id_in_group)) from None

    def get_player_by_role(self, role):
        for p in self.get_players():
            if p.role() == role:
                return p
        raise ValueError('No player with role {}'.format(role))

    def set_players(self, players_list):
        for i, player in enumerate(players_list, start=1):
            player.group = self
            player.id_in_group = i
            player.save()

    def get_neighbor_matrix(self):
        neighbor_matrix = [
            player.get_neighbors()
            for player in self.get_players()
        ]
        
        return neighbor_matrix



    def _set_neighbors(self, links, check_integrity=True):
        """elements in the list can be sublists, or link objects.

        Maybe this should be re-run after before_session_starts() to ensure
        that id_in_links are consistent. Or at least we should validate.


        warning: this deletes the players and any data stored on them
        TODO: we should indicate this in docs
        """
        # first, get players in each group
        try:
            players_flat = [p for g in links for p in g]
        except TypeError:
            raise TypeError(
                'Group matrix must be a list of lists.'
            )
        try:
            matrix_pks = sorted(p.pk for p in players_flat)
            matrix = links
        except AttributeError:
            # if integers, it's OK
            if isinstance(players_flat[0], int):
                matrix = copy.deepcopy(links)
                players = self.get_players()
                for i, row in enumerate(matrix):
                    for j, val in enumerate(row):
                        matrix[i][j] = players[val - 1]
                # assume it's an iterable containing the players
        # Before deleting links, Need to set the foreignkeys to None
        for g in matrix:
            for p in g:
                p.player = None
                p.save()
        self.link_set.all().delete()
        players = list(self.get_players())
        for player, row in zip(players, matrix):
            player.set_neighbors(row)
            player.save()

    def set_neighbors(self, links):
        self._set_neighbors(links, check_integrity=True)

    def _first_round_neighbor_matrix(self):
        players = list(self.get_players())
        players_length = len(players)
        neighbors = []
        for index,player in enumerate(players):
            neighbor_list = []
            neighbor_list.append(players[(index+1) % players_length])
            neighbor_list.append(players[(index+2) % players_length])
            neighbors.append(neighbor_list)
        return neighbors


    def _create_neighbors(self):
        if self.round_number == 1:
            neighbor_matrix = self._first_round_neighbor_matrix()
            self.set_neighbors(neighbor_matrix)
        else:
            self.neighbor_like_round(1)

    def neighbor_like_round(self, round_number):
        previous_round = self.in_round(round_number)
        neighbor_matrix = [
            player.get_neighbors()
            for player in previous_round.get_players()
        ]
        for i, player_list in enumerate(neighbor_matrix):
            for j, link in enumerate(player_list):
                # for every entry (i,j) in the matrix, follow the pointer
                # to the same person in the next round
                neighbor_matrix[i][j] = link.neighbor.in_round(self.round_number)

        # save to DB
        self.set_neighbors(neighbor_matrix)

    def in_round(self, round_number):
        '''You should not use this method if
        you are rearranging groups between rounds.'''

        return type(self).objects.get(
            session=self.session,
            id_in_subsession=self.id_in_subsession,
            round_number=round_number,
        )

    def in_rounds(self, first, last):
        '''You should not use this method if
        you are rearranging groups between rounds.'''

        qs = type(self).objects.filter(
            session=self.session,
            id_in_subsession=self.id_in_subsession,
            round_number__range=(first, last),
        ).order_by('round_number')

        ret = list(qs)
        assert len(ret) == last-first+1
        return ret

    def in_previous_rounds(self):
        return self.in_rounds(1, self.round_number-1)

    def in_all_rounds(self):
        return self.in_previous_rounds() + [self]

    @property
    def _Constants(self):
        return get_models_module(self._meta.app_config.name).Constants

    @classmethod
    def _ensure_required_fields(cls):
        """
        Every ``Group`` model requires a foreign key to the ``Subsession``
        model of the same app.
        """
        subsession_model = '{app_label}.Subsession'.format(
            app_label=cls._meta.app_label)
        subsession_field = models.ForeignKey(subsession_model)
        ensure_field(cls, 'subsession', subsession_field)
