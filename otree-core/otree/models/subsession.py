#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division

import six
from django.db.models import Prefetch
from otree_save_the_change.mixins import SaveTheChange
from otree.db import models
from otree.common_internal import get_models_module
from otree import matching
import copy

ATTRIBUTE_ERROR_MESSAGE = '''
Subsession object has no attribute '{}'. If it is a model field or method,
it must be declared on the Subsession class in models.py.
'''.replace('\n', '')


class BaseSubsession(SaveTheChange, models.Model):
    """Base class for all Subsessions.
    """

    class Meta:
        abstract = True
        ordering = ['pk']
        index_together = ['session', 'round_number']

    session = models.ForeignKey(
        'otree.Session', related_name='%(app_label)s_%(class)s', null=True)

    round_number = models.PositiveIntegerField(
        db_index=True,
        doc='''If this subsession is repeated (i.e. has multiple rounds), this
        field stores the position (index) of this subsession, among subsessions
        in the same app.

        For example, if a session consists of the subsessions:

            [app1, app2, app1, app1, app3]

        Then the round numbers of these subsessions would be:

            [1, 1, 2, 3, 1]

        '''
    )

    def __getattribute__(self, name):
        try:
            return super(BaseSubsession, self).__getattribute__(name)
        except AttributeError:
            raise AttributeError(ATTRIBUTE_ERROR_MESSAGE.format(name)) from None

    def in_rounds(self, first, last):
        qs = type(self).objects.filter(
            session=self.session,
            round_number__range=(first, last),
        ).order_by('round_number')

        return list(qs)

    def in_previous_rounds(self):
        return self.in_rounds(1, self.round_number-1)

    def in_all_rounds(self):
        return self.in_previous_rounds() + [self]

    def name(self):
        return str(self.pk)

    def __unicode__(self):
        return self.name()

    @property
    def app_name(self):
        return self._meta.app_config.name

    def in_round(self, round_number):
        return type(self).objects.get(
            session=self.session,
            round_number=round_number
        )

    def _get_players_per_group_list(self):
        """get a list whose elements are the number of players in each group

        Example: a group of 30 players

        # everyone is in the same group
        [30]

        # 5 groups of 6
        [6, 6, 6, 6, 6,]

        # 2 groups of 5 players, 2 groups of 10 players
        [5, 10, 5, 10] # (you can do this with players_per_group = [5, 10]

        """

        ppg = self._Constants.players_per_group
        subsession_size = self.player_set.count()
        if ppg is None:
            return [subsession_size]

        # if groups have variable sizes, you can put it in a list
        if isinstance(ppg, (list, tuple)):
            assert all(n > 1 for n in ppg)
            group_cycle = ppg
        else:
            assert isinstance(ppg, six.integer_types) and ppg > 1
            group_cycle = [ppg]

        num_group_cycles = subsession_size // sum(group_cycle)
        return group_cycle * num_group_cycles

    def get_groups(self):
        return list(self.group_set.order_by('id_in_subsession'))

    def get_links(self):
        return list(self.link_set.order_by('id_in_player'))

    def get_players(self):
        return list(self.player_set.order_by('pk'))

    def get_group_matrix(self):
        players_prefetch = Prefetch(
            'player_set',
            queryset=self._PlayerClass().objects.order_by('id_in_group'),
            to_attr='_ordered_players')
        return [group._ordered_players
                for group in self.group_set.order_by('id_in_subsession')
                                 .prefetch_related(players_prefetch)]

    def set_group_matrix(self, matrix):
        """
        warning: this deletes the groups and any data stored on them
        TODO: we should indicate this in docs
        """

        try:
            players_flat = [p for g in matrix for p in g]
        except TypeError:
            raise TypeError(
                'Group matrix must be a list of lists.'
            ) from None
        try:
            matrix_pks = sorted(p.pk for p in players_flat)
        except AttributeError:
            # if integers, it's OK
            if isinstance(players_flat[0], int):
                # deep copy so that we don't modify the input arg
                matrix = copy.deepcopy(matrix)
                players_flat = sorted(players_flat)
                if players_flat == list(range(1, len(players_flat) + 1)):
                    players = self.get_players()
                    for i, row in enumerate(matrix):
                        for j, val in enumerate(row):
                            matrix[i][j] = players[val - 1]
                else:
                    raise ValueError(
                        'If you pass a matrix of integers to this function, '
                        'It must contain all integers from 1 to '
                        'the number of players in the subsession.'
                    ) from None
            else:
                raise TypeError(
                    'The elements of the group matrix '
                    'must either be Player objects, or integers.'
                ) from None

        else:
            existing_pks = list(
                self.player_set.values_list(
                    'pk', flat=True
                ).order_by('pk'))
            if matrix_pks != existing_pks:
                raise ValueError(
                    'The group matrix must contain each player '
                    'in the subsession exactly once.'
                )

        # Before deleting groups, Need to set the foreignkeys to None
        self.player_set.update(group=None)
        self.group_set.all().delete()

        GroupClass = self._GroupClass()
        for i, row in enumerate(matrix, start=1):
            group = GroupClass.objects.create(
                subsession=self, id_in_subsession=i,
                session=self.session, round_number=self.round_number)

            group.set_players(row)

    def set_groups(self, matrix):
        '''renamed this to set_group_matrix, but keeping in for compat'''
        return self.set_group_matrix(matrix)

    def check_group_integrity(self):
        ''' should be moved from here to a test case'''
        players = self.player_set.values_list('pk', flat=True)
        players_from_groups = self.player_set.filter(
            group__subsession=self).values_list('pk', flat=True)
        if not set(players) == set(players_from_groups):
            raise Exception('Group integrity check failed')

    def _set_neighbors(self, links, check_integrity=True):
        """elements in the list can be sublists, or link objects.

        Maybe this should be re-run after before_session_starts() to ensure
        that id_in_links are consistent. Or at least we should validate.


        warning: this deletes the players and any data stored on them
        TODO: we should indicate this in docs
        """
        # first, get players in each group
        try:
            players_flat = [p for g in links for p in g]
        except TypeError:
            raise TypeError(
                'Group matrix must be a list of lists.'
            )
        try:
            matrix_pks = sorted(p.pk for p in players_flat)
            matrix = links
        except AttributeError:
            # if integers, it's OK
            if isinstance(players_flat[0], int):
                matrix = copy.deepcopy(links)
                players = self.get_players()
                for i, row in enumerate(matrix):
                    for j, val in enumerate(row):
                        matrix[i][j] = players[val - 1]
                # assume it's an iterable containing the players
        # Before deleting links, Need to set the foreignkeys to None
        for g in matrix:
            for p in g:
                p.player = None
                p.save()
        self.link_set.all().delete()
        players = list(self.get_players())
        for player, row in zip(players, matrix):
            player.set_neighbors(row)
            player.save()

    def get_neighbor_matrix(self):
        neighbor_matrix = [
            player.get_neighbors()
            for player in self.get_players()
        ]
        
        return neighbor_matrix

    def set_neighbor_matrix(self, matrix):
        """
        warning: this deletes the groups and any data stored on them
        TODO: we should indicate this in docs
        """

        try:
            players_flat = [p for g in matrix for p in g]
        except TypeError:
            raise TypeError(
                'Group matrix must be a list of lists.'
            )
        try:
            matrix_pks = sorted(p.pk for p in players_flat)
        except AttributeError:
            # if integers, it's OK
            if isinstance(players_flat[0], int):
                # deep copy so that we don't modify the input arg
                matrix = copy.deepcopy(matrix)
                players_flat = sorted(players_flat)
                if players_flat == list(range(1, len(players_flat) + 1)):
                    players = self.get_players()
                    for i, row in enumerate(matrix):
                        for j, val in enumerate(row):
                            matrix[i][j] = players[val - 1]
                else:
                    raise ValueError(
                        'If you pass a matrix of integers to this function, '
                        'It must contain all integers from 1 to '
                        'the number of players in the subsession.'
                    )
            else:
                raise TypeError(
                    'The elements of the group matrix '
                    'must either be Player objects, or integers.'
                )

        else:
            existing_pks = list(
                self.player_set.values_list(
                    'pk', flat=True
                ).order_by('pk'))
            if matrix_pks != existing_pks:
                raise ValueError(
                    'The group matrix must contain each player '
                    'in the subsession exactly once.'
                )

        # Before deleting groups, Need to set the foreignkeys to None
        self.player_set.update(group=None)
        self.group_set.all().delete()

        GroupClass = self._GroupClass()
        for i, row in enumerate(matrix, start=1):
            group = GroupClass.objects.create(
                subsession=self, id_in_subsession=i,
                session=self.session, round_number=self.round_number)

            group.set_players(row)
            

    def set_neighbors(self, links):
        self._set_neighbors(links, check_integrity=True)

    @property
    def _Constants(self):
        return get_models_module(self._meta.app_config.name).Constants

    def _GroupClass(self):
        return models.get_model(self._meta.app_config.label, 'Group')

    def _PlayerClass(self):
        return models.get_model(self._meta.app_config.label, 'Player')

    def _first_round_group_matrix(self):
        players = list(self.get_players())

        groups = []
        first_player_index = 0

        for group_size in self._get_players_per_group_list():
            groups.append(
                players[first_player_index:first_player_index + group_size]
            )
            first_player_index += group_size
        return groups

    def _first_round_neighbor_matrix(self):
        players = list(self.get_players())
        players_length = len(players)
        neighbors = []
        for index,player in enumerate(players):
            neighbor_list = []
            neighbor_list.append(players[(index+1) % players_length])
            neighbor_list.append(players[(index+2) % players_length])
            neighbors.append(neighbor_list)
        return neighbors

    def _create_groups(self):
        if self.round_number == 1:
            group_matrix = self._first_round_group_matrix()
            self.set_group_matrix(group_matrix)
        else:
            self.group_like_round(1)

    def _create_neighbors(self):
        if self.round_number == 1:
            neighbor_matrix = self._first_round_neighbor_matrix()
            self.set_neighbors(neighbor_matrix)
        else:
            self.neighbor_like_round(1)

    def neighbor_like_round(self, round_number):
        previous_round = self.in_round(round_number)
        neighbor_matrix = [
            player.get_neighbors()
            for player in previous_round.get_players()
        ]
        for i, player_list in enumerate(neighbor_matrix):
            for j, link in enumerate(player_list):
                # for every entry (i,j) in the matrix, follow the pointer
                # to the same person in the next round
                neighbor_matrix[i][j] = link.neighbor.in_round(self.round_number)

        # save to DB
        self.set_neighbors(neighbor_matrix)

    def group_like_round(self, round_number):
        previous_round = self.in_round(round_number)
        group_matrix = [
            group._ordered_players
            for group in previous_round.group_set.order_by(
                'id_in_subsession'
            ).prefetch_related(
                Prefetch(
                    'player_set',
                    queryset=self._PlayerClass().objects.order_by(
                        'id_in_group'),
                    to_attr='_ordered_players'
                )
            )
        ]
        for i, group_list in enumerate(group_matrix):
            for j, player in enumerate(group_list):
                # for every entry (i,j) in the matrix, follow the pointer
                # to the same person in the next round
                group_matrix[i][j] = player.in_round(self.round_number)

        self.set_group_matrix(group_matrix)

    def group_randomly(self, *, fixed_id_in_group=False):
        group_matrix = self.get_group_matrix()
        group_matrix = matching.randomly(
            group_matrix,
            fixed_id_in_group)
        self.set_group_matrix(group_matrix)

    def group_by_rank(self, ranked_list):
        group_matrix = matching.by_rank(
            ranked_list,
            self._Constants.players_per_group
        )
        self.set_group_matrix(group_matrix)


    def _first_round_link_matrix(self):
        players = list(self.get_players())

        links = []
        first_player_index = 0

        for link_size in self._get_players_per_link_list():
            links.append(
                players[first_player_index:first_player_index + link_size]
            )
            first_player_index += link_size
        return links

    def _create_links(self):
        if self.round_number == 1:
            link_matrix = self._first_round_link_matrix()
            self.set_links(link_matrix)
        else:
            self.link_like_round(1)

    def link_like_round(self, round_number):
        previous_round = self.in_round(round_number)
        link_matrix = [
            link.get_players()
            for link in previous_round.get_links()
        ]
        for i, link_list in enumerate(link_matrix):
            for j, player in enumerate(link_list):
                # for every entry (i,j) in the matrix, follow the pointer
                # to the same person in the next round
                link_matrix[i][j] = player.in_round(self.round_number)

        # save to DB
        self.set_links(link_matrix)

    def before_session_starts(self):
        '''This gets called at the beginning of every subsession, before the
        first page is loaded.

        3rd party programmer can put any code here, e.g. to loop through
        players and assign treatment parameters.

        '''
        pass

    def _initialize(self):
        '''wrapper method for self.before_session_starts()'''
        self.before_session_starts()
        # needs to be get_players and get_groups instead of
        # self.player_set.all() because that would just send a new query
        # to the DB
        for p in self.get_players():
            p.save()
        for g in self.get_groups():
            g.save()

        # subsession.save() gets called in the parent method

    def match_players(self, match_name):
        if self.round_number > 1:
            match_function = match_players.MATCHS[match_name]
            pxg = match_function(self)
            for group, players in zip(self.get_groups(), pxg):
                group.set_players(players)
